export function getData() {
  return {
    type: 'SET_DATA_USER'
  }
}

export function getDataSuccess(data) {
  return {
    type: 'SET_DATA_USER_SUCCESS',
    data
  }
}

export function setData(user) {
  return (dispatch) => {
    dispatch(getData())
    dispatch(getDataSuccess(user))
  }
}
