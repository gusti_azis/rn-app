import setUser from './setUser'
import setChat from './setChat'
import fetchHome from './fetchHome'
import fetchWhatsNew from './fetchWhatsNew'
import fetchDesigners from './fetchDesigners'

const actions = {
  ...setUser,
  ...setChat,
  ...fetchHome,
  ...fetchWhatsNew,
  ...fetchDesigners
}

export default actions
