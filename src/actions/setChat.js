export function getData() {
  return {
    type: 'SET_DATA_CHAT'
  }
}

export function getDataSuccess(data) {
  return {
    type: 'SET_DATA_CHAT_SUCCESS',
    data
  }
}

export function setData(chat) {
  return (dispatch) => {
    dispatch(getData())
    dispatch(getDataSuccess(chat))
  }
}
