import getAPI from '../utils/api'

export function getData() {
  return {
    type: 'FETCHING_DATA_DESIGNERS'
  }
}

export function getDataSuccess(data) {
  return {
    type: 'FETCHING_DATA_DESIGNERS_SUCCESS',
    data
  }
}

export function getDataFailure() {
  return {
    type: 'FETCHING_DATA_DESIGNERS_FAILURE'
  }
}

export function fetchData(url) {
  return (dispatch) => {
    dispatch(getData())
    getAPI(url)
      .then((data) => {
        dispatch(getDataSuccess(data))
      })
      .catch((err) => console.log('err:', err))
  }
}
