const api = {
  thumbs: [{
    title: 'CLEARANCE NOW ON',
    subtitle: 'Don’t miss out',
    image: 'https://www.net-a-porter.com/alfresco/nap/webAssets/webPage/homepage-rebuild/desktop/marketing/mobile-promo/2017/07/11/apac/retina_mobilepromo.jpg'
  }, {
    title: 'INSTANT OUTFIT',
    subtitle: 'Meet your throw-on-and-go dress',
    image: 'https://www.net-a-porter.com/alfresco/nap/webAssets/webPage/homepage-rebuild/desktop/marketing/_one/2017/07/13/apac/retina_marketing.jpg'
  }, {
    title: 'FLY WITH THE FASHION PACK',
    subtitle: 'Amanda Shadforth\'s beach chic',
    image: 'https://www.net-a-porter.com/alfresco/nap/webAssets/webPage/homepage-rebuild/desktop/marketing/_two/2017/07/13/apac/retina_marketing.jpg'
  }, {
    title: 'THE EDIT: THE NEW BLACK',
    subtitle: 'The season’s chicest noir pieces',
    image: 'https://www.net-a-porter.com/alfresco/nap/webAssets/webPage/homepage-rebuild/desktop/marketing/mobile-promo/2017/07/13/apac/retina_editpromo.jpg'
  }, {
    title: 'YOUR EVENING UPDATE',
    subtitle: 'Exclusive Caroline Constas & more',
    image: 'https://www.net-a-porter.com/alfresco/nap/webAssets/webPage/homepage-rebuild/desktop/marketing/_three/2017/07/13/apac/retina_marketing.jpg'
  }, {
    title: 'STYLE WELL SPENT',
    subtitle: 'Savvy buys you\'ll love, wear, repeat',
    image: 'https://www.net-a-porter.com/alfresco/nap/webAssets/webPage/homepage-rebuild/desktop/marketing/_four/2017/07/13/apac/retina_marketing.jpg'
  }, {
    title: 'EVENT ACCESSORIES',
    subtitle: 'Find your perfect plus one',
    image: 'https://www.net-a-porter.com/alfresco/nap/webAssets/webPage/homepage-rebuild/desktop/marketing/_five/2017/07/13/apac/retina_marketing.jpg'
  }, {
    title: 'LIVE THE BOHEMIAN DREAM',
    subtitle: 'Chloé headlines the fresh look',
    image: 'https://www.net-a-porter.com/alfresco/nap/webAssets/webPage/homepage-rebuild/desktop/marketing/_six/2017/07/13/apac/retina_marketing.jpg'
  }, {
    title: 'THE SKIRT YOU NEED',
    subtitle: 'It\'s all about asymmetric hems',
    image: 'https://www.net-a-porter.com/alfresco/nap/webAssets/webPage/homepage-rebuild/desktop/marketing/_seven/2017/07/13/apac/retina_marketing.jpg'
  }, {
    title: 'BEAUTY FIX: BACK TO NATURE',
    subtitle: 'Discover the power of plants',
    image: 'https://www.net-a-porter.com/alfresco/nap/webAssets/webPage/homepage-rebuild/desktop/marketing/_eight/2017/07/13/apac/retina_marketing.jpg'
  }],

  products: [{
    name: 'Lace-trimmed printed jacquard gown',
    designer: 'Etro',
    price: '$3,110',
    image: 'https://cache.net-a-porter.com/images/products/910318/910318_in_l.jpg'
  }, {
    name: 'Velvet pumps',
    designer: 'Prada',
    price: '$850',
    image: 'https://cache.net-a-porter.com/images/products/918927/918927_in_l.jpg'
  }, {
    name: 'Bolce floral-print silk dress',
    designer: 'Vilshenko',
    price: '$1,828',
    image: 'https://cache.net-a-porter.com/images/products/918010/918010_in_l.jpg'
  }, {
    name: 'Bellechasse leather shoulder bag',
    designer: 'Saint Laurent',
    price: '$1,283',
    image: 'https://cache.net-a-porter.com/images/products/917594/917594_in_l.jpg'
  }, {
    name: 'Striped metallic wool-blend midi dress',
    designer: 'Fendi',
    price: '$981',
    image: 'https://cache.net-a-porter.com/images/products/894867/894867_in_l.jpg'
  }, {
    name: 'Casati embellished leather loafers',
    designer: 'Nicholas Kirkwood',
    price: '$674',
    image: 'https://cache.net-a-porter.com/images/products/888989/888989_in_l.jpg'
  }, {
    name: 'Boycott double-breasted crushed-velvet blazer',
    designer: 'Ellery',
    price: '$2,544',
    image: 'https://cache.net-a-porter.com/images/products/915001/915001_in_l.jpg'
  }, {
    name: 'Printed mid-rise skinny jeans',
    designer: 'Etro',
    price: '$367',
    image: 'https://cache.net-a-porter.com/images/products/910342/910342_in_l.jpg'
  }, {
    name: 'Mykonos II round-frame gold-tone sunglasses',
    designer: 'Illesteva',
    price: '$140',
    image: 'https://cache.net-a-porter.com/images/products/935597/935597_in_l.jpg'
  }, {
    name: 'Metallic striped wool-blend sweater',
    designer: 'Fendi',
    price: '$846',
    image: 'https://cache.net-a-porter.com/images/products/894874/894874_in_l.jpg'
  }, {
    name: 'Bow-embellished suede and velvet pumps',
    designer: 'Miu Miu',
    price: '$883',
    image: 'https://cache.net-a-porter.com/images/products/895816/895816_in_l.jpg'
  }, {
    name: 'Intarsia cotton sweater',
    designer: 'Givenchy',
    price: '$987',
    image: 'https://cache.net-a-porter.com/images/products/912930/912930_in_m2.jpg'
  }],

  designers: [{
    name: '111SKIN'
  }, {
    name: '3.1 PHILLIP LIM'
  }, {
    name: '37 ACTIVES'
  }, {
    name: '3LAB'
  }, {
    name: '3X1'
  }, {
    name: 'A.P.C. ATELIER DE PRODUCTION ET DE CRÉATION'
  }, {
    name: 'A.W.A.K.E.'
  }, {
    name: 'AAMAYA BY PRIYANKA'
  }, {
    name: 'ACNE STUDIOS'
  }, {
    name: 'ADAM LIPPES'
  }, {
    name: 'ADEAM'
  }, {
    name: 'ADIDAS ORIGINALS'
  }, {
    name: 'ADIDAS ORIGINALS BY ALEXANDER WANG'
  }, {
    name: 'ADIDAS PERFORMANCE'
  }, {
    name: 'ADRIANA DEGREAS'
  }, {
    name: 'AERIN BEAUTY'
  }, {
    name: 'AERIN'
  }, {
    name: 'AFRICAN BOTANICS'
  }, {
    name: 'AGENT PROVOCATEUR'
  }, {
    name: 'ALANUI'
  }, {
    name: 'ALAÏA'
  }, {
    name: 'ALAÏA BEAUTY'
  }, {
    name: 'ALESSANDRA RICH'
  }, {
    name: 'ALEXACHUNG'
  }, {
    name: 'ALEXANDER MCQUEEN'
  }, {
    name: 'ALEXANDER WANG'
  }, {
    name: 'ALEXANDRE BIRMAN'
  }, {
    name: 'ALICE + OLIVIA'
  }, {
    name: 'ALICE CICOLINI'
  }, {
    name: 'ALISON LOU'
  }, {
    name: 'ALIX'
  }, {
    name: 'ALLIES OF SKIN'
  }, {
    name: 'ALLUDE'
  }, {
    name: 'ALTUZARRA'
  }, {
    name: 'AMANDA LACEY'
  }, {
    name: 'AMRAPALI'
  }, {
    name: 'ANNIE COSTELLO BROWN'
  }, {
    name: 'ANCIENT GREEK SANDALS'
  }, {
    name: 'ANDERSON\'S'
  }, {
    name: 'ANDREA FOHRMAN'
  }, {
    name: 'ANINE BING'
  }, {
    name: 'ANISSA KERMICHE'
  }, {
    name: 'ANITA KO'
  }, {
    name: 'ANJUNA'
  }, {
    name: 'ANN DEMEULEMEESTER'
  }, {
    name: 'ANNA SUI'
  }, {
    name: 'ANNE SEMONIN'
  }, {
    name: 'ANTIK BATIK'
  }, {
    name: 'ANTONIO BERARDI'
  }, {
    name: 'ANYA HINDMARCH'
  }, {
    name: 'APA BEAUTY'
  }, {
    name: 'APIECE APART'
  }, {
    name: 'APL ATHLETIC PROPULSION LABS'
  }, {
    name: 'AQUAZZURA'
  }, {
    name: 'ARAKS'
  }, {
    name: 'ARGENTUM APOTHECARY'
  }, {
    name: 'ARIAT'
  }, {
    name: 'ARME DE L\'AMOUR'
  }, {
    name: 'AROMATHERAPY ASSOCIATES'
  }, {
    name: 'ARTIS BRUSH'
  }, {
    name: 'ASHISH'
  }, {
    name: 'ASSOULINE'
  }, {
    name: 'AUTUMN CPH'
  }, {
    name: 'ATLEIN'
  }, {
    name: 'ATM ANTHONY THOMAS MELILLO'
  }, {
    name: 'ATP ATELIER'
  }, {
    name: 'ATTICO'
  }, {
    name: 'AURELIA PROBIOTIC SKINCARE'
  }, {
    name: 'AURÉLIE BIDERMANN'
  }, {
    name: 'AXEL ARIGATO'
  }, {
    name: 'B&O PLAY'
  }, {
    name: 'BABJADES'
  }, {
    name: 'BALENCIAGA'
  }, {
    name: 'BALLET BEAUTIFUL'
  }, {
    name: 'BALMAIN'
  }, {
    name: 'BALMAIN PARIS HAIR COUTURE'
  }, {
    name: 'BAMFORD'
  }, {
    name: 'BARBARA CASASOLA'
  }, {
    name: 'BASERANGE'
  }, {
    name: 'BASSIKE'
  }, {
    name: 'BBROWBAR'
  }, {
    name: 'BEAUFILLE'
  }, {
    name: 'BEAUTYBLENDER'
  }, {
    name: 'BELLA FREUD'
  }, {
    name: 'BELLA FREUD PARFUM'
  }, {
    name: 'BIOEFFECT'
  }, {
    name: 'BIYAN'
  }, {
    name: 'BODYISM'
  }, {
    name: 'BOMBKI'
  }, {
    name: 'BORGO DE NOR'
  }, {
    name: 'BOTTEGA VENETA'
  }, {
    name: 'BOUTIQUE MOSCHINO'
  }, {
    name: 'BOYY'
  }, {
    name: 'BRANDON MAXWELL'
  }, {
    name: 'BROOKE GREGSON'
  }, {
    name: 'BROTHER VELLIES'
  }, {
    name: 'BRUNELLO CUCINELLI'
  }, {
    name: 'BUCCELLATI'
  }, {
    name: 'BUILDING BLOCK'
  }, {
    name: 'BULY 1803'
  }, {
    name: 'BURBERRY'
  }, {
    name: 'BURBERRY BEAUTY'
  }, {
    name: 'BY MALENE BIRGER'
  }, {
    name: 'BY TERRY'
  }, {
    name: 'BYREDO'
  }, {
    name: 'CALI DREAMING'
  }, {
    name: 'CALVIN KLEIN COLLECTION'
  }, {
    name: 'CALVIN KLEIN UNDERWEAR'
  }, {
    name: 'CAMILLA'
  }, {
    name: 'CANADA GOOSE'
  }, {
    name: 'CAMI NYC'
  }, {
    name: 'CARAVANA'
  }, {
    name: 'CARINE GILSON'
  }, {
    name: 'CARMEN MARCH'
  }, {
    name: 'CAROLINA BUCCI'
  }, {
    name: 'CAROLINE CONSTAS'
  }, {
    name: 'CARVEN'
  }, {
    name: 'CASTAÑER'
  }, {
    name: 'CAUDALIE'
  }, {
    name: 'CATBIRD'
  }, {
    name: 'CÉDRIC CHARLIER'
  }, {
    name: 'CEFINN'
  }, {
    name: 'CELIA DRAGOUNI'
  }, {
    name: 'CHALAYAN'
  }, {
    name: 'CHAN LUU'
  }, {
    name: 'CHANTECAILLE'
  }, {
    name: 'CHARLOTTE CHESNAIS'
  }, {
    name: 'CHARLOTTE OLYMPIA'
  }, {
    name: 'CHARLOTTE TILBURY'
  }, {
    name: 'CHINTI AND PARKER'
  }, {
    name: 'CHLOÉ'
  }, {
    name: 'CHRISTIAN LOUBOUTIN'
  }, {
    name: 'CHRISTOPHE ROBIN'
  }, {
    name: 'CHRISTOPHER KANE'
  }, {
    name: 'CHURCH\'S'
  }, {
    name: 'CIRE TRUDON'
  }, {
    name: 'CLARE V'
  }, {
    name: 'CLEANSE BY LAUREN NAPIER'
  }, {
    name: 'CLOE CASSANDRO'
  }, {
    name: 'CLU'
  }, {
    name: 'CO'
  }, {
    name: 'COLBERT MD'
  }, {
    name: 'COLOR ME'
  }, {
    name: 'COLOR WOW'
  }, {
    name: 'COMMANDO'
  }, {
    name: 'COMME DES GARÇONS'
  }, {
    name: 'COMME DES GARÇONS COMME DES GARÇONS'
  }, {
    name: 'COMME DES GARÇONS GIRL'
  }, {
    name: 'COMMON PROJECTS'
  }, {
    name: 'COQUI COQUI'
  }, {
    name: 'CORNELIA WEBB'
  }, {
    name: 'COSABELLA'
  }, {
    name: 'CULT GAIA'
  }, {
    name: 'CURRENT/ELLIOTT'
  }, {
    name: 'CUSHNIE ET OCHS'
  }, {
    name: 'CUTLER AND GROSS'
  }, {
    name: 'CVC STONES'
  }, {
    name: 'DANIELA VILLEGAS'
  }, {
    name: 'DANNIJO'
  }, {
    name: 'DANSE LENTE'
  }, {
    name: 'DAVINES'
  }, {
    name: 'DAVID MALLETT'
  }, {
    name: 'DINOSAUR DESIGNS'
  }, {
    name: 'DE MAMIEL'
  }, {
    name: 'DEBORAH LIPPMANN'
  }, {
    name: 'DELFINA DELETTREZ'
  }, {
    name: 'DERMARCHÉ LABS'
  }, {
    name: 'DIANE KORDAS'
  }, {
    name: 'DIANE VON FURSTENBERG'
  }, {
    name: 'DION LEE'
  }, {
    name: 'DITA'
  }, {
    name: 'DKNY'
  }, {
    name: 'DMONDAINE'
  }, {
    name: 'DODO BAR OR'
  }, {
    name: 'DOLCE & GABBANA'
  }, {
    name: 'DORATEYMUR'
  }, {
    name: 'DR NIGMA TALIB'
  }, {
    name: 'DR SEBAGH'
  }, {
    name: 'DR. BARBARA STURM'
  }, {
    name: 'DR. JACKSON\'S'
  }, {
    name: 'DR. DENNIS GROSS SKINCARE'
  }, {
    name: 'DRAPER JAMES'
  }, {
    name: 'DUSKII'
  }, {
    name: 'EARTH TU FACE'
  }, {
    name: 'EBERJEY'
  }, {
    name: 'EDDIE BORGO'
  }, {
    name: 'EDDIE HARROP'
  }, {
    name: 'EDIE PARKER'
  }, {
    name: 'EDWARD BESS'
  }, {
    name: 'ELIE SAAB'
  }, {
    name: 'ELIZABETH AND JAMES'
  }, {
    name: 'ELIZABETH ARDEN'
  }, {
    name: 'ELLE MACPHERSON BODY'
  }, {
    name: 'ELLERY'
  }, {
    name: 'ELLIS BROOKLYN'
  }, {
    name: 'ELLIS FAAS'
  }, {
    name: 'EMBRYOLISSE'
  }, {
    name: 'EMILIA WICKSTEAD'
  }, {
    name: 'EMILIO PUCCI'
  }, {
    name: 'EMMA PAKE'
  }, {
    name: 'EQUIPMENT'
  }, {
    name: 'ERDEM'
  }, {
    name: 'ERES'
  }, {
    name: 'ERICKSON BEAMON'
  }, {
    name: 'ERNO LASZLO'
  }, {
    name: 'ESTEBAN CORTAZAR'
  }, {
    name: 'ESTELLE & THILD'
  }, {
    name: 'ÉTOILE ISABEL MARANT'
  }, {
    name: 'ETRO'
  }, {
    name: 'EUGENIA KIM'
  }, {
    name: 'EVE LOM'
  }, {
    name: 'EVE SNOW'
  }, {
    name: 'EYEKO'
  }, {
    name: 'EYTYS'
  }, {
    name: 'FACETASM'
  }, {
    name: 'FALKE'
  }, {
    name: 'FALKE ERGONOMIC SPORT SYSTEM'
  }, {
    name: 'FASHION FORMS'
  }, {
    name: 'FREDA BANANA'
  }, {
    name: 'FENDI'
  }, {
    name: 'FIGUE'
  }, {
    name: 'FLAGPOLE'
  }, {
    name: 'FLEUR DU MAL'
  }, {
    name: 'FELLA'
  }, {
    name: 'FOREO'
  }, {
    name: 'FORNASETTI'
  }, {
    name: 'FOUNDRAE'
  }, {
    name: 'FRAME'
  }, {
    name: 'FRANCESCO RUSSO'
  }, {
    name: 'FRASCO MIRRORS'
  }, {
    name: 'FRED LEIGHTON'
  }, {
    name: 'FRENDS'
  }, {
    name: 'F.R.S FOR RESTLESS SLEEPERS'
  }, {
    name: 'FUSALP'
  }, {
    name: 'GABRIELA HEARST'
  }, {
    name: 'GANNI'
  }, {
    name: 'GARETH PUGH'
  }, {
    name: 'GENTLE MONSTER'
  }, {
    name: 'GEORGIA ALICE'
  }, {
    name: 'GHD'
  }, {
    name: 'GIAMBATTISTA VALLI'
  }, {
    name: 'GIANVITO ROSSI'
  }, {
    name: 'GIUSEPPE ZANOTTI'
  }, {
    name: 'GIVENCHY'
  }, {
    name: 'GLOBE-TROTTER'
  }, {
    name: 'GLOSSIER'
  }, {
    name: 'GOEN J'
  }, {
    name: 'GOLDEN GOOSE DELUXE BRAND'
  }, {
    name: 'GOLDFADEN MD'
  }, {
    name: 'GROWN ALCHEMIST'
  }, {
    name: 'GUCCI'
  }, {
    name: 'GUCCI FOR NET-A-PORTER'
  }, {
    name: 'GÜL HÜRGEL'
  }, {
    name: 'HAIDER ACKERMANN'
  }, {
    name: 'HALFPENNY LONDON'
  }, {
    name: 'HALSTON HERITAGE'
  }, {
    name: 'HAMMAMAS'
  }, {
    name: 'HAMPTON SUN'
  }, {
    name: 'HANEY'
  }, {
    name: 'HANKY PANKY'
  }, {
    name: 'HANRO'
  }, {
    name: 'HARRIS WHARF LONDON'
  }, {
    name: 'HATCH'
  }, {
    name: 'HEAL GEL'
  }, {
    name: 'HEIDI KLEIN'
  }, {
    name: 'HELMUT LANG'
  }, {
    name: 'HERVÉ LÉGER'
  }, {
    name: 'HILLIER BARTLEY'
  }, {
    name: 'HOLZWEILER'
  }, {
    name: 'HOURGLASS'
  }, {
    name: 'HOUSE OF HOLLAND'
  }, {
    name: 'HUNZA G'
  }, {
    name: 'HVN'
  }, {
    name: 'I.D. SARRIERI'
  }, {
    name: 'I+I'
  }, {
    name: 'ILA'
  }, {
    name: 'ILEANA MAKRI'
  }, {
    name: 'ILIA'
  }, {
    name: 'ILLAMASQUA'
  }, {
    name: 'ILLESTEVA'
  }, {
    name: 'IPPOLITA'
  }, {
    name: 'IRO'
  }, {
    name: 'ISA ARFEN'
  }, {
    name: 'ISABEL MARANT'
  }, {
    name: 'ISABEL MARANT, ÉTOILE'
  }, {
    name: 'IWC SCHAFFHAUSEN'
  }, {
    name: 'J BRAND'
  }, {
    name: 'J.CREW'
  }, {
    name: 'J.W.ANDERSON'
  }, {
    name: 'JABONERIA MARIANELLA'
  }, {
    name: 'JACQUEMUS'
  }, {
    name: 'JAMES PERSE'
  }, {
    name: 'JAMES READ'
  }, {
    name: 'JANAVI'
  }, {
    name: 'JANESSA LEONE'
  }, {
    name: 'JASON WU'
  }, {
    name: 'JEMMA WYNNE'
  }, {
    name: 'JENNIE KWON DESIGNS'
  }, {
    name: 'JENNIFER BEHR'
  }, {
    name: 'JENNIFER FISHER'
  }, {
    name: 'JENNIFER MEYER'
  }, {
    name: 'JENNY PACKHAM'
  }, {
    name: 'JÉRÔME DREYFUSS'
  }, {
    name: 'JIL SANDER'
  }, {
    name: 'JIMMY CHOO'
  }, {
    name: 'JO LOVES'
  }, {
    name: 'JOHANNA ORTIZ'
  }, {
    name: 'JONATHAN SIMKHAI'
  }, {
    name: 'JOSEPH'
  }, {
    name: 'JOSHUA SANDERS'
  }, {
    name: 'JOYA'
  }, {
    name: 'JOOSTRICOT'
  }, {
    name: 'JUAN CARLOS OBANDO'
  }, {
    name: 'JUNYA WATANABE'
  }, {
    name: 'KAHINA GIVING BEAUTY'
  }, {
    name: 'KARA'
  }, {
    name: 'KAREN WALKER'
  }, {
    name: 'KARL DONOGHUE'
  }, {
    name: 'KARL LAGERFELD'
  }, {
    name: 'KARLA COLLETTO'
  }, {
    name: 'KAYU'
  }, {
    name: 'KENNETH JAY LANE'
  }, {
    name: 'KENZO'
  }, {
    name: 'KEVYN AUCOIN'
  }, {
    name: 'KIINI'
  }, {
    name: 'KIKI DE MONTPARNASSE'
  }, {
    name: 'KJAER WEIS'
  }, {
    name: 'KJUS'
  }, {
    name: 'KORA ORGANICS BY MIRANDA KERR'
  }, {
    name: 'KOZA'
  }, {
    name: 'KREWE'
  }, {
    name: 'KAT BURKI'
  }, {
    name: 'KATERINA MAKRIYIANNI'
  }, {
    name: 'L\'AGENCE'
  }, {
    name: 'L\'AGENT BY AGENT PROVOCATEUR'
  }, {
    name: 'L\'ETOILE SPORT'
  }, {
    name: 'LA LIGNE'
  }, {
    name: 'LA PERLA'
  }, {
    name: 'LACROIX'
  }, {
    name: 'LAFCO'
  }, {
    name: 'LANCER'
  }, {
    name: 'LANO - LIPS HANDS ALL OVER'
  }, {
    name: 'LANVIN'
  }, {
    name: 'LARKSPUR & HAWK'
  }, {
    name: 'LARSSON & JENNINGS'
  }, {
    name: 'LASHFOOD'
  }, {
    name: 'LAUREN B. BEAUTY'
  }, {
    name: 'LE LABO'
  }, {
    name: 'LE METIER DE BEAUTE'
  }, {
    name: 'LE SPECS'
  }, {
    name: 'LEGOLOGY'
  }, {
    name: 'LELA ROSE'
  }, {
    name: 'LELET NY'
  }, {
    name: 'LEMAIRE'
  }, {
    name: 'LEMLEM'
  }, {
    name: 'LEONOR GREYL'
  }, {
    name: 'LILAH B.'
  }, {
    name: 'LINDA FARROW'
  }, {
    name: 'LINGUA FRANCA'
  }, {
    name: 'LIPSTICK QUEEN'
  }, {
    name: 'LISA EISNER'
  }, {
    name: 'LISA MARIE FERNANDEZ'
  }, {
    name: 'LITO'
  }, {
    name: 'LITTLE LIFFNER'
  }, {
    name: 'LIVE THE PROCESS'
  }, {
    name: 'LNDR'
  }, {
    name: 'LOEFFLER RANDALL'
  }, {
    name: 'LOEWE'
  }, {
    name: 'LONELY'
  }, {
    name: 'LONG BY VALERY JOSEPH'
  }, {
    name: 'LOREN STEWART'
  }, {
    name: 'LOVESHACKFANCY'
  }, {
    name: 'LOVE STORIES'
  }, {
    name: 'LUCAS HUGH'
  }, {
    name: 'LUXE CITY GUIDES'
  }, {
    name: 'M2MALLETIER'
  }, {
    name: 'MABU BY MARIA BK'
  }, {
    name: 'MADELEINE THOMPSON'
  }, {
    name: 'MADEWELL'
  }, {
    name: 'MADEWORN'
  }, {
    name: 'MAGDA BUTRYM'
  }, {
    name: 'MAGGIE MARILYN'
  }, {
    name: 'MAISON MARGIELA'
  }, {
    name: 'MAISON MICHEL'
  }, {
    name: 'MAIYET'
  }, {
    name: 'MAJE'
  }, {
    name: 'MAKE BEAUTY'
  }, {
    name: 'MALIN + GOETZ'
  }, {
    name: 'MALONE SOULIERS'
  }, {
    name: 'MANEBI'
  }, {
    name: 'MANSUR GAVRIEL'
  }, {
    name: 'MANU ATELIER'
  }, {
    name: 'MARA HOFFMAN'
  }, {
    name: 'MARC JACOBS'
  }, {
    name: 'MARC JACOBS BEAUTY'
  }, {
    name: 'MARCH11'
  }, {
    name: 'MARCHESA'
  }, {
    name: 'MARCHESA NOTTE'
  }, {
    name: 'MARCO DE VINCENZO'
  }, {
    name: 'MARGARET DABBS LONDON'
  }, {
    name: 'MARIA BLACK'
  }, {
    name: 'MARIA LA ROSA'
  }, {
    name: 'MARK CROSS'
  }, {
    name: 'MARKUS LUPFER'
  }, {
    name: 'MARNI'
  }, {
    name: 'MARQUES\' ALMEIDA'
  }, {
    name: 'MARIA TASH'
  }, {
    name: 'MARTINIANO'
  }, {
    name: 'MARVIS'
  }, {
    name: 'MARY KATRANTZOU'
  }, {
    name: 'MARYSIA'
  }, {
    name: 'MATTER & HOME'
  }, {
    name: 'MATTHEW WILLIAMSON'
  }, {
    name: 'MAULI RITUALS'
  }, {
    name: 'MAX MARA'
  }, {
    name: 'MAY LINDSTROM'
  }, {
    name: 'MCQ ALEXANDER MCQUEEN'
  }, {
    name: 'MCQUEEN, ALEXANDER'
  }, {
    name: 'MELISSA JOY MANNING'
  }, {
    name: 'MELISSA ODABASH'
  }, {
    name: 'MERCEDES CASTILLO'
  }, {
    name: 'MERCHANT ARCHIVE'
  }, {
    name: 'MES DEMOISELLES'
  }, {
    name: 'MICHAEL KORS COLLECTION'
  }, {
    name: 'MICHAEL LO SORDO'
  }, {
    name: 'MICHAEL MICHAEL KORS'
  }, {
    name: 'MICHAEL VAN CLARKE'
  }, {
    name: 'MICHELLE MASON'
  }, {
    name: 'MIGUELINA'
  }, {
    name: 'M.I.H JEANS'
  }, {
    name: 'MIKOH'
  }, {
    name: 'MIO SKINCARE'
  }, {
    name: 'MIRA MIKATI'
  }, {
    name: 'MISSONI'
  }, {
    name: 'MISSONI HOME'
  }, {
    name: 'MIU MIU'
  }, {
    name: 'MIZUKI'
  }, {
    name: 'MM6 MAISON MARGIELA'
  }, {
    name: 'MONCLER'
  }, {
    name: 'MONCLER GRENOBLE'
  }, {
    name: 'MONICA VINADER'
  }, {
    name: 'MONREAL LONDON'
  }, {
    name: 'MONSE'
  }, {
    name: 'MOON BOOT'
  }, {
    name: 'MOSCHINO'
  }, {
    name: 'MOTHER'
  }, {
    name: 'MOTHER OF PEARL'
  }, {
    name: 'MR & MRS ITALY'
  }, {
    name: 'MR BY MAN REPELLER'
  }, {
    name: 'MSGM'
  }, {
    name: 'MARIE-STELLA-MARIS'
  }, {
    name: 'MUGLER'
  }, {
    name: 'MULBERRY'
  }, {
    name: 'MUSA'
  }, {
    name: 'MUUN'
  }, {
    name: 'MZ SKIN'
  }, {
    name: 'MZ WALLACE'
  }, {
    name: 'NANNACAY'
  }, {
    name: 'NANNETTE DE GASPÉ'
  }, {
    name: 'NARCISO RODRIGUEZ'
  }, {
    name: 'NATASHA SCHWEITZER'
  }, {
    name: 'NATURA BISSÉ'
  }, {
    name: 'NEEDLE & THREAD'
  }, {
    name: 'NEOM ORGANICS'
  }, {
    name: 'NEOUS'
  }, {
    name: 'NEST FRAGRANCES'
  }, {
    name: 'NET-A-PORTER KITS'
  }, {
    name: 'NICHOLAS KIRKWOOD'
  }, {
    name: 'NIKE'
  }, {
    name: 'NILI LOTAN'
  }, {
    name: 'NO KA\'OI'
  }, {
    name: 'NO. 21'
  }, {
    name: 'NOOR FARES'
  }, {
    name: 'NORMA KAMALI'
  }, {
    name: 'NURSE JAMIE'
  }, {
    name: 'NYDG SKINCARE'
  }, {
    name: 'OFF-WHITE'
  }, {
    name: 'OLIVIA VON HALLE'
  }, {
    name: 'OLYMPIA ACTIVEWEAR'
  }, {
    name: 'OLYMPIA LE-TAN'
  }, {
    name: 'OMOROVICZA'
  }, {
    name: 'ONIRA ORGANICS'
  }, {
    name: 'OPENING CEREMONY'
  }, {
    name: 'ORIBE'
  }, {
    name: 'ORIGINAL & MINERAL'
  }, {
    name: 'OSCAR DE LA RENTA'
  }, {
    name: 'OSCAR TIYE'
  }, {
    name: 'OSKIA'
  }, {
    name: 'ON THE ISLAND BY MARIOS SCHWAB'
  }, {
    name: 'OUAI HAIRCARE'
  }, {
    name: 'P.E NATION'
  }, {
    name: 'PALLAS'
  }, {
    name: 'PALOMA BLUE'
  }, {
    name: 'PAMELA LOVE'
  }, {
    name: 'PAPER LONDON'
  }, {
    name: 'PASCALE MONVOISIN'
  }, {
    name: 'PAT MCGRATH LABS'
  }, {
    name: 'PAUL & JOE'
  }, {
    name: 'PAUL ANDREW'
  }, {
    name: 'PEDRO GARCIA'
  }, {
    name: 'PERCOSSI PAPI'
  }, {
    name: 'PERFECT MOMENT'
  }, {
    name: 'PERRICONE MD'
  }, {
    name: 'PETAR PETROV'
  }, {
    name: 'PETER PILOTTO'
  }, {
    name: 'PHAIDON'
  }, {
    name: 'PHILIP B'
  }, {
    name: 'PHILIP KINGSLEY'
  }, {
    name: 'PHILIP TREACY'
  }, {
    name: 'PHILOSOPHY DI LORENZO SERAFINI'
  }, {
    name: 'PIAGET'
  }, {
    name: 'PIERRE BALMAIN'
  }, {
    name: 'PIPPA HOLT'
  }, {
    name: 'PIPPA SMALL'
  }, {
    name: 'PLACE NATIONALE'
  }, {
    name: 'POMELLATO'
  }, {
    name: 'POMMADE DIVINE'
  }, {
    name: 'PORTER MAGAZINE'
  }, {
    name: 'PRADA'
  }, {
    name: 'PREEN BY THORNTON BREGAZZI'
  }, {
    name: 'PRISM'
  }, {
    name: 'PROENZA SCHOULER'
  }, {
    name: 'PROTAGONIST'
  }, {
    name: 'PURELY PERFECT'
  }, {
    name: 'R13'
  }, {
    name: 'RACHEL ZOE'
  }, {
    name: 'RADEN'
  }, {
    name: 'RADICAL SKINCARE'
  }, {
    name: 'RAG & BONE'
  }, {
    name: 'RAHUA'
  }, {
    name: 'RAQUEL ALLEGRA'
  }, {
    name: 'RAY-BAN'
  }, {
    name: 'REBECCA VALLANCE'
  }, {
    name: 'RE/DONE'
  }, {
    name: 'REDVALENTINO'
  }, {
    name: 'REEBOK'
  }, {
    name: 'REEM ACRA'
  }, {
    name: 'REFORMATION'
  }, {
    name: 'REJINA PYO'
  }, {
    name: 'REN SKINCARE'
  }, {
    name: 'RENÉ CAOVILLA'
  }, {
    name: 'REPOSSI'
  }, {
    name: 'REVERENCE DE BASTIEN'
  }, {
    name: 'RICK OWENS'
  }, {
    name: 'RIME ARODAKY'
  }, {
    name: 'RIXO LONDON'
  }, {
    name: 'RIZZOLI'
  }, {
    name: 'RMS BEAUTY'
  }, {
    name: 'ROBERT CLERGERIE'
  }, {
    name: 'ROCHELLE SARA'
  }, {
    name: 'ROCKINS'
  }, {
    name: 'RODIN'
  }, {
    name: 'ROKSANDA'
  }, {
    name: 'ROLAND MOURET'
  }, {
    name: 'RONALD VAN DER KEMP'
  }, {
    name: 'ROSAMOSARIO'
  }, {
    name: 'ROSANTICA'
  }, {
    name: 'ROSETTA GETTY'
  }, {
    name: 'ROSIE ASSOULIN'
  }, {
    name: 'ROSSANO FERRETTI PARMA'
  }, {
    name: 'RETROSUPERFUTURE'
  }, {
    name: 'RUPERT SANDERSON'
  }, {
    name: 'RYAN STORER'
  }, {
    name: 'RYE'
  }, {
    name: 'SACAI'
  }, {
    name: 'SACHAJUAN'
  }, {
    name: 'SAINT LAURENT'
  }, {
    name: 'SAKS POTTS'
  }, {
    name: 'SALONI'
  }, {
    name: 'SAM EDELMAN'
  }, {
    name: 'SANAYI313'
  }, {
    name: 'SANDY LIANG'
  }, {
    name: 'SANSOEURS'
  }, {
    name: 'SANTA MARIA NOVELLA'
  }, {
    name: 'SARA BATTAGLIA'
  }, {
    name: 'SARAH & SEBASTIAN'
  }, {
    name: 'SARAH CHAPMAN'
  }, {
    name: 'SEA'
  }, {
    name: 'SEE BY CHLOÉ'
  }, {
    name: 'SELF-PORTRAIT'
  }, {
    name: 'SENSI STUDIO'
  }, {
    name: 'SENTEURS D\'ORIENT'
  }, {
    name: 'SHOW BEAUTY'
  }, {
    name: 'SHRIMPS'
  }, {
    name: 'SIMON MILLER'
  }, {
    name: 'SIMONE ROCHA'
  }, {
    name: 'SISTER BY SIBLING'
  }, {
    name: 'SJYP STEVE J & YONI P'
  }, {
    name: 'SK-II'
  }, {
    name: 'SASKIA DIEZ'
  }, {
    name: 'SKIN'
  }, {
    name: 'SLEEPY JONES'
  }, {
    name: 'SLIP'
  }, {
    name: 'SMITH & CULT'
  }, {
    name: 'SMYTHSON'
  }, {
    name: 'SOLACE LONDON'
  }, {
    name: 'SOLEIL TOUJOURS'
  }, {
    name: 'SOLID AND STRIPED'
  }, {
    name: 'SONIA RYKIEL'
  }, {
    name: 'SOPHIA WEBSTER'
  }, {
    name: 'SOPHIE ANDERSON'
  }, {
    name: 'SOPHIE BILLE BRAHE'
  }, {
    name: 'SOPHIE BUHAI'
  }, {
    name: 'SOVERAL'
  }, {
    name: 'SPANX'
  }, {
    name: 'SPLENDID'
  }, {
    name: 'SUNDAY SOMEWHERE'
  }, {
    name: 'STAUD'
  }, {
    name: 'STELLA MCCARTNEY'
  }, {
    name: 'STEPHEN WEBSTER'
  }, {
    name: 'STUDIOSARAH'
  }, {
    name: 'STUART WEITZMAN'
  }, {
    name: 'SU-MAN SKINCARE'
  }, {
    name: 'SUNDAY RILEY'
  }, {
    name: 'SURRATT BEAUTY'
  }, {
    name: 'SUSANNE KAUFMANN'
  }, {
    name: 'SUZANNE KALAN'
  }, {
    name: 'T BY ALEXANDER WANG'
  }, {
    name: 'TABITHA SIMMONS'
  }, {
    name: 'TABULA RASA'
  }, {
    name: 'TALITHA'
  }, {
    name: 'TASCHEN'
  }, {
    name: 'TATA HARPER'
  }, {
    name: 'TATCHA'
  }, {
    name: 'TEMPERLEY LONDON'
  }, {
    name: 'TEMPTU'
  }, {
    name: 'TENOVERTEN'
  }, {
    name: 'THAMES & HUDSON'
  }, {
    name: 'THE BEACH PEOPLE'
  }, {
    name: 'THE CASE FACTORY'
  }, {
    name: 'THE ELDER STATESMAN'
  }, {
    name: 'THE GREAT'
  }, {
    name: 'THE LAUNDRESS'
  }, {
    name: 'THE PERFUMER\'S STORY BY AZZI GLASSER'
  }, {
    name: 'THE ROW'
  }, {
    name: 'THE UPSIDE'
  }, {
    name: 'THEORY'
  }, {
    name: 'THIERRY LASRY'
  }, {
    name: 'THIS WORKS'
  }, {
    name: 'THREE GRACES LONDON'
  }, {
    name: 'TIBI'
  }, {
    name: 'TIFFANY & CO.'
  }, {
    name: 'THREE J NYC'
  }, {
    name: 'TKEES'
  }, {
    name: 'TOD\'S'
  }, {
    name: 'TOGA'
  }, {
    name: 'TOM DAXON'
  }, {
    name: 'TOM DIXON'
  }, {
    name: 'TOM FORD'
  }, {
    name: 'TOMASINI'
  }, {
    name: 'TOME'
  }, {
    name: 'TOPSHOP SNO'
  }, {
    name: 'TOPSHOP UNIQUE'
  }, {
    name: 'TORY SPORT'
  }, {
    name: 'TORY BURCH'
  }, {
    name: 'TOTÊME'
  }, {
    name: 'TRACIE MARTYN'
  }, {
    name: 'TRUSS'
  }, {
    name: 'TUMI'
  }, {
    name: 'TUXE BODYWEAR'
  }, {
    name: 'ULLA JOHNSON'
  }, {
    name: 'VALENTINO'
  }, {
    name: 'VANESSA SEWARD'
  }, {
    name: 'VICTOR GLEMAUD'
  }, {
    name: 'VERNON FRANÇOIS'
  }, {
    name: 'VERONICA BEARD'
  }, {
    name: 'VERSACE'
  }, {
    name: 'VERSO'
  }, {
    name: 'VERSUS VERSACE'
  }, {
    name: 'VETEMENTS'
  }, {
    name: 'VICTORIA BECKHAM'
  }, {
    name: 'VICTORIA BECKHAM DENIM'
  }, {
    name: 'VICTORIA BECKHAM ESTÉE LAUDER'
  }, {
    name: 'VICTORIA, VICTORIA BECKHAM'
  }, {
    name: 'VILSHENKO'
  }, {
    name: 'VINCE'
  }, {
    name: 'VINTNER\'S DAUGHTER'
  }, {
    name: 'VIVIENNE WESTWOOD ANGLOMANIA'
  }, {
    name: 'VIX'
  }, {
    name: 'VOTARY'
  }, {
    name: 'WANDA NYLON'
  }, {
    name: 'WANDER BEAUTY'
  }, {
    name: 'WARD WHILLAS'
  }, {
    name: 'WESTWARD LEANING X OLIVIA PALERMO'
  }, {
    name: 'WOLFORD'
  }, {
    name: 'WWAKE'
  }, {
    name: 'Y/PROJECT'
  }, {
    name: 'YEPREM'
  }, {
    name: 'YUNOTME'
  }, {
    name: 'YOSUZI'
  }, {
    name: 'YUMMIE BY HEATHER THOMSON'
  }, {
    name: 'YVES SALOMON'
  }, {
    name: 'YVONNE LÉON'
  }, {
    name: 'ZANA BAYNE'
  }, {
    name: 'ZELENS'
  }, {
    name: 'ZENSATION'
  }, {
    name: 'ZEUS+DIONE'
  }, {
    name: 'ZIIP BEAUTY'
  }, {
    name: 'ZIMMERMANN'
  }, {
    name: 'ZUHAIR MURAD'
  }]
}

export default (url) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      return resolve(api[url])
    }, 1000)
  })
}
