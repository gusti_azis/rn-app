import React from 'react'
import { AppRegistry } from 'react-native'
import { Provider } from 'react-redux'
import { applyMiddleware, createStore } from 'redux'
import thunk from 'redux-thunk'
import Permissions from 'react-native-permissions'
import OneSignal from 'react-native-onesignal'

import reducers from './reducers'
import AppNavigator from './AppNavigator'

const store = createStore(reducers, applyMiddleware(thunk))
const isDebuggingInChrome = __DEV__ && !!window.navigator.userAgent

class App extends React.Component {
  store = store

  componentWillMount() {
    Permissions.request('photo')
    Permissions.request('camera')
    Permissions.request('location')

    OneSignal.addEventListener('received', this.onReceived)
    OneSignal.addEventListener('opened', this.onOpened)
    OneSignal.addEventListener('registered', this.onRegistered)
    OneSignal.addEventListener('ids', this.onIds)
    OneSignal.setSubscription(true)
    OneSignal.sendTag('email', 'gratcysegala@gmail.com')
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived)
    OneSignal.removeEventListener('opened', this.onOpened)
    OneSignal.removeEventListener('registered', this.onRegistered)
    OneSignal.removeEventListener('ids', this.onIds)
  }

  onReceived(notification) {
    console.log('onReceived: ', notification)
  }

  onOpened(openResult) {
    console.log('onOpened', openResult)
  }

  onRegistered(notifData) {
    console.log('onRegistered', notifData)
  }

  onIds(device) {
    console.log('onIds', device)
  }

  render() {
    if (isDebuggingInChrome) {
      window.store = store
    }

    return (
      <Provider store={this.store}>
        <AppNavigator />
      </Provider>
    )
  }
}

AppRegistry.registerComponent('App', () => App)

export default App
