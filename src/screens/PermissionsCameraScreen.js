import React from 'react'
import { StyleSheet, View, Text } from 'react-native'

class PermissionsCameraScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Camera Permission Granted</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF'
  }
})

export default PermissionsCameraScreen
