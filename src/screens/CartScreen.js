import React from 'react'
import { StyleSheet, View, Text } from 'react-native'

class CartScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={[styles.text, styles.head]}>YOUR CART IS CURRENTLY EMPTY</Text>
        <Text style={styles.text}>WHY NOT CHECK OUT WHAT'S NEW?</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    paddingRight: '20%',
    paddingLeft: '20%'
  },
  text: {
    fontSize: 11,
    fontWeight: '500',
    textAlign: 'center',
    letterSpacing: 2,
    lineHeight: 16
  },
  head: {
    marginBottom: 20,
    fontFamily: 'Gill Sans',
    fontSize: 18,
    fontWeight: '400',
    lineHeight: 24
  }
})

export default CartScreen
