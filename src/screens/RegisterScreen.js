import React from 'react'
import { StyleSheet, View, TextInput, Text, TouchableHighlight } from 'react-native'

class RegisterScreen extends React.Component {
  state = {
    first: '',
    sur: '',
    email: '',
    password: '',
    repassword: ''
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.box}>
          <TextInput
            style={styles.input}
            onChangeText={(text) => this.setState({ first: text })}
            value={this.state.text}
            placeholder="First Name"
            autoFocus={true}
            underlineColorAndroid={'transparent'}
          />
          <TextInput
            style={styles.input}
            onChangeText={(text) => this.setState({ sur: text })}
            value={this.state.text}
            placeholder="Surname"
            underlineColorAndroid={'transparent'}
          />
          <TextInput
            style={styles.input}
            onChangeText={(text) => this.setState({ email: text })}
            value={this.state.text}
            placeholder="Email Address"
            underlineColorAndroid={'transparent'}
          />
          <TextInput
            style={styles.input}
            onChangeText={(text) => this.setState({ password: text })}
            value={this.state.text}
            placeholder="Password"
            underlineColorAndroid={'transparent'}
          />
          <TextInput
            style={styles.input}
            onChangeText={(text) => this.setState({ repassword: text })}
            value={this.state.text}
            placeholder="Retype your password"
            underlineColorAndroid={'transparent'}
          />
          <TouchableHighlight style={styles.button}>
            <Text style={styles.title}>REGISTER</Text>
          </TouchableHighlight>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F5F5'
  },
  box: {
    paddingVertical: 25,
    paddingHorizontal: 50
  },
  input: {
    marginBottom: 15,
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderColor: '#CCCCCC',
    borderWidth: 1,
    letterSpacing: 10,
    fontFamily: 'Gill Sans',
    fontSize: 16,
    backgroundColor: '#FFFFFF'
  },
  button: {
    marginVertical: 20,
    paddingVertical: 10,
    paddingHorizontal: 15,
    alignItems: 'center',
    backgroundColor: '#212121'
  },
  title: {
    fontFamily: 'Gill Sans',
    fontWeight: '600',
    letterSpacing: 2,
    color: '#FFFFFF'
  }
})

export default RegisterScreen
