import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, ScrollView, ListView, Image, Text, TouchableHighlight } from 'react-native'
import { connect } from 'react-redux'

import { fetchData } from '../actions/fetchWhatsNew'

class WhatsNewScreen extends React.Component {
  static propTypes = {
    fetchData: PropTypes.func.isRequired,
    dataWhatsNew: PropTypes.object.isRequired
  }

  state = {
    products: new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
  }

  componentWillMount() {
    this.props.fetchData('products')
  }

  componentWillReceiveProps(nextProps) {
    const data = nextProps.dataWhatsNew.data

    if (data.length) {
      this.setState({ products: this.state.products.cloneWithRows(data) })
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <ListView
            contentContainerStyle={styles.grid}
            dataSource={this.state.products}
            renderRow={(product) => (
              <TouchableHighlight style={styles.item}>
                <View style={styles.view}>
                  <View style={styles.canvas}>
                    <Image source={{ uri: product.image }} resizeMode="cover" style={styles.image} />
                  </View>
                  <View style={styles.caption}>
                    <Text style={styles.designer}>{product.designer.toUpperCase()}</Text>
                    <Text style={styles.name}>{product.name}</Text>
                    <Text style={styles.price}>{product.price}</Text>
                  </View>
                </View>
              </TouchableHighlight>
            )}
          />
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  grid: {
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    flex: 1
  },
  item: {
    width: '50%',
    marginBottom: 15
  },
  view: {
    alignItems: 'center'
  },
  image: {
    marginVertical: 15,
    width: 115,
    height: 172.5
  },
  caption: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    marginTop: 5,
    alignSelf: 'stretch'
  },
  designer: {
    marginBottom: 5,
    fontFamily: 'Gill Sans',
    fontWeight: '600',
    fontSize: 12,
    letterSpacing: 2
  },
  name: {
    fontSize: 12
  },
  price: {
    fontSize: 12,
    marginTop: 10,
    fontWeight: '600'
  }
})

const mapStateToProps = state => ({
  dataWhatsNew: state.dataWhatsNew
})

const mapDispatchToProps = dispatch => ({
  fetchData: (url) => dispatch(fetchData(url))
})

export default connect(mapStateToProps, mapDispatchToProps)(WhatsNewScreen)
