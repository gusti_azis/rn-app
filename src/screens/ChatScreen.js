import React from 'react'
import { Platform, StyleSheet, View } from 'react-native'
import SocketIOClient from 'socket.io-client'
import { GiftedChat } from 'react-native-gifted-chat'

class ChatScreen extends React.Component {
  socket = SocketIOClient('http://192.168.2.75:31129')
  debug = true

  constructor(props) {
    super(props)

    this.state = {
      messages: [],
      user: {
        nick: 'Gratcy',
        email: 'gratcysegala@gmail.com',
        msg: 'Hi!',
        dep_id: '0',
        order_id: '0',
        member_id: '33333',
        unique_id: '88888',
        agent: Platform.OS
      },
      chat: {
        chat_id: this.debug ? '61' : '',
        instance_id: this.debug ? '0' : ''
      },
      hash: '',
      userId: 0
    }

    const s = this.state
    const u = s.user

    this.socket.on('connect', () => { console.log('Socket IO Connected') })
    this.socket.on('disconnect', () => { console.log('Socket IO Disconnected') })

    this.socket.on('regischat-' + u.dep_id + '-' + u.unique_id, this.onRegisterChat.bind(this))
    this.socket.on('resultmsgs', this.onResultMsgs.bind(this))
    this.socket.on('operatortyping', this.onOperatorTyping.bind(this))
    this.socket.on('operatorpostedmessage', this.onOperatorPostedMessage.bind(this))

    console.log('onRegisterChat', 'regischat-' + u.dep_id + '-' + u.unique_id)
  }

  componentDidMount() {
    // this.socket.emit('registerchat', JSON.stringify({ data: this.state.user }))

    const c = this.state.chat
    this.socket.emit('join', JSON.stringify(c))
    this.socket.emit('requestmsg', JSON.stringify(c))

    console.log(this.socket)
  }

  componentWillUnmount() {
    this.socket.emit('leave', JSON.stringify(this.state.chat))
  }

  onRegisterChat(response) {
    this.setState({
      chat: {
        chat_id: response.chat_id,
        instance_id: response.instance_id
      },
      hash: response.hash
    })

    console.log('onRegisterChat', response)
  }

  onResultMsgs(response) {
    const messages = []
    for (let i = 0; i < response.length; i++) {
      const r = response[i]

      if (r.user_id < 0) {
        continue
      }

      messages.push({
        _id: r.id,
        text: r.msg,
        createdAt: r.time,
        user: {
          _id: r.user_id,
          name: r.user_id ? 'Admin' : 'Guest'
        }
      })
    }

    this.setState({ messages: messages.reverse() })

    console.log('onResultMsgs', response)
  }

  onUserPostedMessage(messages) {
    const chat = this.state.chat
    const message = {
      chat_id: chat.chat_id,
      instance_id: chat.instance_id,
      hash: this.state.hash,
      msg: messages[0].text
    }

    this.socket.emit('userstartedpostmessage', JSON.stringify(chat))
    this.socket.emit('userpostedmessage', JSON.stringify(message))
    this.storeMessages(messages)

    console.log('onUserPostedMessage', message)
  }

  onUserTyping(text) {
    const chat = this.state.chat
    const data = {
      chat_data: {
        chat_id: chat.chat_id,
        status: true,
        msg: text
      },
      instance_id: chat.instance_id
    }

    this.socket.emit('usertyping', JSON.stringify(data))

    console.log('onUserTyping', data)
  }

  onOperatorTyping(response) {
    console.log('onOperatorTyping', response)
  }

  onOperatorPostedMessage(response) {
    this.storeMessages([{
      _id: this.state.messages.length,
      text: response.msg,
      createdAt: new Date(),
      user: {
        _id: 1,
        name: 'Admin'
      }
    }])

    console.log('onOperatorPostedMessage', response)
  }

  storeMessages(messages) {
    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, messages)
      }
    })
  }

  render() {
    const user = { _id: this.state.userId }
    const msgs = this.state.messages

    return (
      <View style={styles.container}>
        <GiftedChat
          messages={msgs}
          onSend={(msgs) => this.onUserPostedMessage(msgs)}
          onInputTextChanged={(text) => this.onUserTyping(text)}
          user={user}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  }
})

export default ChatScreen
