import React from 'react'
import { StyleSheet, View, Text } from 'react-native'

class PermissionsPhotosScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Photos Permission Granted</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF'
  }
})

export default PermissionsPhotosScreen
