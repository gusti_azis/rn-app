import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, TextInput, Text, TouchableHighlight } from 'react-native'
import { connect } from 'react-redux'

import { setData } from '../actions/setUser'

class SignInScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    setData: PropTypes.func.isRequired
  }

  state = {
    email: '',
    password: '',
    err: false
  }

  handleSubmitSignIn() {
    const p = this.props
    const s = this.state

    this.setState({ err: false })
    if (s.email && s.password) {
      p.setData({ email: s.email, password: s.password })
      p.navigation.navigate('Main')
    } else {
      this.setState({ err: true })
    }
  }

  render() {
    const navigation = this.props.navigation

    return (
      <View style={styles.container}>
        <View style={styles.box}>
          {this.state.err && (
            <Text style={styles.err}>Password / Email combination incorrect</Text>
          )}
          <TextInput
            style={styles.input}
            onChangeText={(text) => this.setState({ email: text })}
            value={this.state.text}
            placeholder="Email Address"
            autoFocus={true}
            underlineColorAndroid={'transparent'}
          />
          <TextInput
            style={styles.input}
            onChangeText={(text) => this.setState({ password: text })}
            value={this.state.text}
            placeholder="Password"
            secureTextEntry={true}
            underlineColorAndroid={'transparent'}
          />
          <TouchableHighlight style={styles.link} onPress={() => navigation.navigate('ForgotPassword')} underlayColor={'#F5F5F5'}>
            <Text style={styles.forgot}>Forgotten your password?</Text>
          </TouchableHighlight>
          <TouchableHighlight style={styles.button} onPress={() => this.handleSubmitSignIn()}>
            <Text style={styles.title}>SIGN IN</Text>
          </TouchableHighlight>
          <TouchableHighlight style={styles.link} onPress={() => navigation.navigate('Register')} underlayColor={'#F5F5F5'}>
            <Text>
              <Text style={styles.forgot}>Don't have an account? </Text>
              <Text style={styles.bold}>Register here</Text>
            </Text>
          </TouchableHighlight>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F5F5'
  },
  box: {
    flex: 1,
    paddingVertical: 25,
    paddingHorizontal: 50
  },
  input: {
    marginBottom: 15,
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderColor: '#CCCCCC',
    borderWidth: 1,
    letterSpacing: 10,
    fontFamily: 'Gill Sans',
    fontSize: 16,
    backgroundColor: '#FFFFFF'
  },
  button: {
    marginVertical: 20,
    paddingVertical: 10,
    paddingHorizontal: 15,
    alignItems: 'center',
    backgroundColor: '#212121'
  },
  title: {
    fontFamily: 'Gill Sans',
    fontWeight: '600',
    letterSpacing: 2,
    color: '#FFFFFF'
  },
  link: {
    alignItems: 'center'
  },
  forgot: {
    fontFamily: 'Gill Sans',
    fontWeight: '200'
  },
  bold: {
    fontFamily: 'Gill Sans',
    fontWeight: '400'
  },
  err: {
    fontFamily: 'Gill Sans',
    fontWeight: '200',
    color: 'red',
    marginBottom: 20
  }
})

const mapStateToProps = state => ({
})

const mapDispatchToProps = dispatch => ({
  setData: (user) => dispatch(setData(user))
})

export default connect(mapStateToProps, mapDispatchToProps)(SignInScreen)
