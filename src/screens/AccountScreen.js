import React from 'react'
import PropTypes from 'prop-types'
import { Platform, StyleSheet, View, ScrollView, Text, TouchableHighlight, Alert } from 'react-native'
import Permissions from 'react-native-permissions'
import { openSettings } from 'react-native-open-settings'

class AccountScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  }

  handlePressPermissions(value, screen) {
    const auth = 'authorized'

    Permissions.check(value)
      .then(res => {
        if (res !== auth) {
          if (Platform.OS === 'android') {
            if (res === 'restricted') {
              this.alert(value)
            } else {
              Permissions.request(value)
                .then(req => {
                  if (req === auth) {
                    this.props.navigation.navigate(screen)
                  }
                })
            }
          } else {
            this.alert(value)
          }
        } else {
          this.props.navigation.navigate(screen)
        }
      })
  }

  alert(value) {
    Alert.alert(
      '"App" Would Like to Access Your ' + this.capitalized(value),
      '',
      [
        { text: 'Cancel' },
        { text: 'Settings', onPress: () => openSettings() }
      ]
    )
  }

  capitalized(string) {
    return string.charAt(0).toUpperCase() + string.slice(1)
  }


  render() {
    const navigation = this.props.navigation

    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.grid}>
            <TouchableHighlight style={styles.button} onPress={() => navigation.navigate('SignIn')}>
              <Text style={styles.title}>SIGN IN / REGISTER</Text>
            </TouchableHighlight>
            <View style={styles.separator} />
            <TouchableHighlight style={styles.link} onPress={() => navigation.navigate('SignIn')} underlayColor={'#F5F5F5'}>
              <Text style={styles.text}>My Orders</Text>
            </TouchableHighlight>
            <View style={styles.separator} />
            <TouchableHighlight style={styles.link} onPress={() => navigation.navigate('SignIn')} underlayColor={'#F5F5F5'}>
              <Text style={styles.text}>My Account Details</Text>
            </TouchableHighlight>
            <View style={styles.separator} />
            <TouchableHighlight style={styles.link} onPress={() => navigation.navigate('SignIn')} underlayColor={'#F5F5F5'}>
              <Text style={styles.text}>My Addresses for Billing and Shipping</Text>
            </TouchableHighlight>
            <View style={styles.separator} />
            <TouchableHighlight style={styles.link} onPress={() => navigation.navigate('SignIn')} underlayColor={'#F5F5F5'}>
              <Text style={styles.text}>My Wish List</Text>
            </TouchableHighlight>
            <View style={styles.separator} />
            <TouchableHighlight style={styles.link} onPress={() => navigation.navigate('Chat')} underlayColor={'#F5F5F5'}>
              <Text style={styles.text}>My Chat</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.grid}>
            <Text style={styles.head}>PERMISSIONS</Text>
            <TouchableHighlight style={styles.link} onPress={() => this.handlePressPermissions('location', 'PermissionsLocation')} underlayColor={'#F5F5F5'}>
              <Text style={styles.text}>Location</Text>
            </TouchableHighlight>
            <View style={styles.separator} />
            <TouchableHighlight style={styles.link} onPress={() => this.handlePressPermissions('photo', 'PermissionsPhotos')} underlayColor={'#F5F5F5'}>
              <Text style={styles.text}>Photos / Media / Files</Text>
            </TouchableHighlight>
            <View style={styles.separator} />
            <TouchableHighlight style={styles.link} onPress={() => this.handlePressPermissions('camera', 'PermissionsCamera')} underlayColor={'#F5F5F5'}>
              <Text style={styles.text}>Camera</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.grid}>
            <Text style={styles.head}>SETTINGS</Text>
            <TouchableHighlight style={styles.link}>
              <Text style={styles.text}>Notifications</Text>
            </TouchableHighlight>
            <View style={styles.separator} />
            <TouchableHighlight style={styles.link}>
              <Text style={styles.text}>Change Country</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.grid}>
            <TouchableHighlight style={styles.link}>
              <Text style={styles.text}>Subscribe to Porter Magazine</Text>
            </TouchableHighlight>
            <View style={styles.separator} />
            <TouchableHighlight style={styles.link}>
              <Text style={styles.text}>Scan Porter Magazine</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.grid}>
            <Text style={styles.head}>HELP AND INFORMATION</Text>
            <TouchableHighlight style={styles.link}>
              <Text style={styles.text}>Returns and Exchanges</Text>
            </TouchableHighlight>
            <View style={styles.separator} />
            <TouchableHighlight style={styles.link}>
              <Text style={styles.text}>Shipping Information</Text>
            </TouchableHighlight>
            <View style={styles.separator} />
            <TouchableHighlight style={styles.link}>
              <Text style={styles.text}>Payment Security</Text>
            </TouchableHighlight>
            <View style={styles.separator} />
            <TouchableHighlight style={styles.link}>
              <Text style={styles.text}>Privacy Policy</Text>
            </TouchableHighlight>
            <View style={styles.separator} />
            <TouchableHighlight style={styles.link}>
              <Text style={styles.text}>Cookie Policy</Text>
            </TouchableHighlight>
            <View style={styles.separator} />
            <TouchableHighlight style={styles.link}>
              <Text style={styles.text}>Terms and Conditions</Text>
            </TouchableHighlight>
            <View style={styles.separator} />
            <TouchableHighlight style={styles.link}>
              <Text style={styles.text}>FAQs</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.grid}>
            <Text style={styles.head}>CONTACT US</Text>
            <View style={styles.link}>
              <Text style={styles.text}>Help, Advice & Feedback</Text>
              <Text style={[styles.text, styles.gap]}>Available 24 hours a day, 7 days a week in over 14 languages.</Text>
            </View>
            <TouchableHighlight style={styles.button}>
              <Text style={styles.title}>EMAIL US</Text>
            </TouchableHighlight>
            <View style={styles.separator} />
            <TouchableHighlight style={styles.link}>
              <Text style={styles.text}>Contact Details</Text>
            </TouchableHighlight>
          </View>
          <View style={[styles.grid, styles.last]}>
            <TouchableHighlight style={styles.link}>
              <Text style={styles.text}>App Version 1.0.0</Text>
            </TouchableHighlight>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F5F5'
  },
  grid: {
    flex: 1,
    marginBottom: 30,
    backgroundColor: '#FFFFFF'
  },
  link: {
    paddingVertical: 15,
    paddingHorizontal: 20
  },
  head: {
    paddingVertical: 15,
    paddingHorizontal: 20,
    fontFamily: 'Georgia',
    fontSize: 15,
    letterSpacing: 1
  },
  button: {
    flex: 1,
    marginVertical: 15,
    marginHorizontal: 20,
    paddingVertical: 15,
    paddingHorizontal: 20,
    alignItems: 'center',
    backgroundColor: '#212121'
  },
  title: {
    fontFamily: 'Gill Sans',
    fontWeight: '600',
    letterSpacing: 2,
    color: '#FFFFFF'
  },
  text: {
    fontFamily: 'Gill Sans',
    fontSize: 15
  },
  gap: {
    marginTop: 10
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#CCCCCC'
  },
  last: {
    marginBottom: 0
  }
})

export default AccountScreen
