import React from 'react'
import { StyleSheet, View } from 'react-native'
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'

class PermissionsLocationScreen extends React.Component {
  state = {
    region: {
      latitude: -0.789275,
      longitude: 113.921327,
      latitudeDelta: 71.70840294234583,
      longitudeDelta: 47.82781273126602
    },
    markers: []
  }

  regions(lat, lng, distance) {
    distance = distance / 2
    const circumference = 40075
    const oneDegreeOfLatitudeInMeters = 111.32 * 1000
    const angularDistance = distance / circumference

    const latitudeDelta = distance / oneDegreeOfLatitudeInMeters
    const longitudeDelta = Math.abs(Math.atan2(
      Math.sin(angularDistance) * Math.cos(lat),
      Math.cos(angularDistance) - Math.sin(lat) * Math.sin(lat)
    ))

    return {
      latitude: lat,
      longitude: lng,
      latitudeDelta,
      longitudeDelta
    }
  }

  onRegionChange(region) {
    console.log(region)
  }

  render() {
    const sr = this.state.region

    return (
      <View style ={styles.container}>
        <View style ={styles.canvas}>
          <MapView
            provider={PROVIDER_GOOGLE}
            style={styles.map}
            region={{
              latitude: sr.latitude,
              longitude: sr.longitude,
              latitudeDelta: sr.latitudeDelta,
              longitudeDelta: sr.longitudeDelta
            }}
            onRegionChange={this.onRegionChange}
            loadingEnabled={true}
          >
            {this.state.markers.map((marker, i) => (
              <MapView.Marker
                key={i}
                coordinate={marker.latlng}
                title={marker.title}
                description={marker.description}
              />
            ))}
          </MapView>
        </View>
        <View style={styles.search}>
          <GooglePlacesAutocomplete
            placeholder="Search Places"
            placeholderTextColor="#AAAAAA"
            onPress={(data, details) => {
              const len = data.terms.length
              const loc = details.geometry.location
              const reg = this.regions(loc.lat, loc.lng, 25000 / len)

              this.setState({
                region: reg,
                markers: [{
                  latlng: { latitude: loc.lat, longitude: loc.lng },
                  title: details.name,
                  description: details.vicinity
                }]
              })
              console.log(data, details, reg)
            }}
            fetchDetails={true}
            query={{
              key: 'AIzaSyBnDpUYPhjGi_IQFStCWwYm7FI1FGPQ4f8',
              components: 'country:id',
              type: 'establishment'
            }}
            styles={{
              textInputContainer: styles.textInputContainer,
              textInput: styles.textInput,
              poweredContainer: styles.poweredContainer,
              row: styles.row,
              separator: styles.separator
            }}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  canvas: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  search: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: '#FFFFFF'
  },
  textInputContainer: {
    backgroundColor: '#FFFFFF',
    borderTopColor: '#EEEEEE',
    borderBottomColor: '#EEEEEE'
  },
  textInput: {
    borderRadius: 0,
    fontSize: 14
  },
  poweredContainer: {
    opacity: 0,
    height: 0,
    padding: 0
  },
  row: {
    flex: 1
  },
  separator: {
    backgroundColor: '#EEEEEE'
  }
})

export default PermissionsLocationScreen
