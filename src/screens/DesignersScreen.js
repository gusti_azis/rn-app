import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, ListView, Text, TouchableHighlight } from 'react-native'
import { connect } from 'react-redux'

import { fetchData } from '../actions/fetchDesigners'

class DesignersScreen extends React.Component {
  static propTypes = {
    fetchData: PropTypes.func.isRequired,
    dataDesigners: PropTypes.object.isRequired
  }

  constructor(props) {
    super(props)

    const getSectionData = (dataBlob, sectionId) => dataBlob[sectionId]
    const getRowData = (dataBlob, sectionId, rowId) => dataBlob[`${rowId}`]

    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
      sectionHeaderHasChanged: (s1, s2) => s1 !== s2,
      getSectionData,
      getRowData
    })


    this.state = {
      designers: ds.cloneWithRowsAndSections({}, [], [])
    }
  }

  componentWillMount() {
    this.props.fetchData('designers')
  }

  componentWillReceiveProps(nextProps) {
    const data = nextProps.dataDesigners.data

    if (data.length) {
      const { dataBlob, sectionIds, rowIds } = this.resturctureData(data)
      this.setState({ designers: this.state.designers.cloneWithRowsAndSections(dataBlob, sectionIds, rowIds) })
    }
  }

  resturctureData(data) {
    const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('')

    const dataBlob = {}
    const sectionIds = []
    const rowIds = []

    for (let sectionId = 0; sectionId < alphabet.length; sectionId++) {
      const currentChar = alphabet[sectionId]
      const designers = data.filter((designer) => designer.name.toUpperCase().indexOf(currentChar) === 0)

      if (designers.length > 0) {
        sectionIds.push(sectionId)
        dataBlob[sectionId] = { character: currentChar }
        rowIds.push([])

        for (let i = 0; i < designers.length; i++) {
          const rowId = `${sectionId}:${i}`

          rowIds[rowIds.length - 1].push(rowId)
          dataBlob[rowId] = designers[i]
        }
      }
    }

    return { dataBlob, sectionIds, rowIds }
  }

  render() {
    return (
      <View style={styles.container}>
        <ListView
          style={styles.container}
          dataSource={this.state.designers}
          renderRow={(designer) => (
            <TouchableHighlight>
              <View style={styles.item}>
                <Text style={styles.name}>{designer.name}</Text>
              </View>
            </TouchableHighlight>
          )}
          renderSeparator={(sectionId, rowId) => (
            <View key={rowId} style={styles.separator} />
          )}
          renderSectionHeader={(sectionData) => (
            <View style={[styles.item, styles.shade]}>
              <Text style={styles.character}>{sectionData.character}</Text>
            </View>
          )}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  item: {
    flex: 1,
    flexDirection: 'row',
    paddingVertical: 10,
    paddingHorizontal: 15
  },
  shade: {
    flexDirection: null,
    backgroundColor: '#F5F5F5'
  },
  name: {
    fontFamily: 'Gill Sans',
    letterSpacing: 1
  },
  character: {
    fontFamily: 'Georgia',
    fontSize: 24
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#FFFFFF'
  }
})

const mapStateToProps = state => ({
  dataDesigners: state.dataDesigners
})

const mapDispatchToProps = dispatch => ({
  fetchData: (url) => dispatch(fetchData(url))
})

export default connect(mapStateToProps, mapDispatchToProps)(DesignersScreen)
