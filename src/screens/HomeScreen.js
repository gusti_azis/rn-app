import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, ScrollView, Image, Text, TouchableHighlight } from 'react-native'
import { connect } from 'react-redux'

import { fetchData } from '../actions/fetchHome'

class HomeScreen extends React.Component {
  static propTypes = {
    fetchData: PropTypes.func.isRequired,
    dataHome: PropTypes.object.isRequired
  }

  componentWillMount() {
    this.props.fetchData('thumbs')
  }

  render() {
    const thumbs = this.props.dataHome

    return (
      <View style={thumbs.isFetching ? [styles.container, styles.center] : styles.container}>
        {thumbs.isFetching && <Text>Loading</Text>}
        {thumbs.data.length ? (
          <ScrollView>
            {thumbs.data.map((thumb, i) =>
              <TouchableHighlight key={i}>
                <View style={styles.item}>
                  <View style={styles.canvas}>
                    <Image source={{ uri: thumb.image }} resizeMode="cover" style={styles.image} />
                  </View>
                  <View style={styles.caption}>
                    <Text style={styles.title}>{thumb.title}</Text>
                    <Text>{thumb.subtitle}</Text>
                  </View>
                </View>
              </TouchableHighlight>
            )}
          </ScrollView>
        ) : null}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  item: {
    flex: 1,
    alignItems: 'center',
    position: 'relative'
  },
  canvas: {
    flex: 1,
    alignSelf: 'stretch'
  },
  image: {
    flex: 1,
    alignItems: 'center',
    width: null,
    height: 240
  },
  caption: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 15,
    paddingHorizontal: 15,
    alignItems: 'center',
    alignSelf: 'stretch',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderTopColor: '#EEEEEE',
    borderBottomColor: '#EEEEEE'
  },
  title: {
    marginBottom: 5,
    fontFamily: 'Gill Sans',
    fontWeight: '400',
    fontSize: 14,
    letterSpacing: 2
  }
})

const mapStateToProps = state => ({
  dataHome: state.dataHome
})

const mapDispatchToProps = dispatch => ({
  fetchData: (url) => dispatch(fetchData(url))
})

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
