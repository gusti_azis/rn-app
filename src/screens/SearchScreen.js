import React from 'react'
import { Platform, StyleSheet, View, TextInput } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'

const ios = Platform.OS === 'ios'

class SearchScreen extends React.Component {
  state = {
    text: ''
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.search}>
          <View style={styles.viewIcon}>
            <Ionicons name="ios-search-outline" size={20} style={styles.icon} />
          </View>
          <View style={styles.viewInput}>
            <TextInput
              style={styles.input}
              onChangeText={(text) => this.setState({ text })}
              value={this.state.text}
              placeholder="SEARCH"
              autoFocus={true}
              underlineColorAndroid={'transparent'}
            />
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  search: {
    padding: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: '#FFFFFF',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderTopColor: '#EEEEEE',
    borderBottomColor: '#EEEEEE'
  },
  viewIcon: {
    flexDirection: 'column'
  },
  viewInput: {
    flex: 1,
    flexDirection: 'column'
  },
  icon: {
    paddingTop: ios ? 8 : 14,
    paddingRight: 12.5,
    paddingBottom: ios ? 6 : 12,
    paddingLeft: 12.5
  },
  input: {
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    borderWidth: 0,
    letterSpacing: 10,
    fontSize: 13
  }
})

export default SearchScreen
