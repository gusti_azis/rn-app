import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, TextInput, Text, TouchableHighlight } from 'react-native'

class SignInScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  }

  state = {
    email: ''
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.box}>
          <Text style={styles.forgot}>
            <Text>Please enter your email address and tap on submit.</Text>
            <Text>We will then send you an email containing a link that you can tap to create a new password.</Text>
          </Text>
          <TextInput
            style={styles.input}
            onChangeText={(text) => this.setState({ email: text })}
            value={this.state.text}
            placeholder="Email Address"
            autoFocus={true}
            underlineColorAndroid={'transparent'}
          />
          <TouchableHighlight style={styles.button}>
            <Text style={styles.title}>SUBMIT</Text>
          </TouchableHighlight>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F5F5'
  },
  box: {
    paddingVertical: 25,
    paddingHorizontal: 50
  },
  input: {
    marginBottom: 15,
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderColor: '#CCCCCC',
    borderWidth: 1,
    letterSpacing: 10,
    fontFamily: 'Gill Sans',
    fontSize: 16,
    backgroundColor: '#FFFFFF'
  },
  button: {
    marginVertical: 20,
    paddingVertical: 10,
    paddingHorizontal: 15,
    alignItems: 'center',
    backgroundColor: '#212121'
  },
  title: {
    fontFamily: 'Gill Sans',
    fontWeight: '600',
    letterSpacing: 2,
    color: '#FFFFFF'
  },
  forgot: {
    fontFamily: 'Gill Sans',
    fontWeight: '200',
    marginBottom: 20
  }
})

export default SignInScreen
