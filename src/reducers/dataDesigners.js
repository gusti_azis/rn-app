const initialState = {
  data: [],
  isFetching: false,
  error: false
}

export default function dataDesigners(state = initialState, action) {
  switch (action.type) {
    case 'FETCHING_DATA_DESIGNERS':
      return {
        ...state,
        data: [],
        isFetching: true
      }
    case 'FETCHING_DATA_DESIGNERS_SUCCESS':
      return {
        ...state,
        data: action.data,
        isFetching: false
      }
    case 'FETCHING_DATA_DESIGNERS_FAILURE':
      return {
        ...state,
        isFetching: false,
        error: true
      }
    default:
      return state
  }
}
