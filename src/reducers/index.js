import { combineReducers } from 'redux'
import dataUser from './dataUser'
import dataChat from './dataChat'
import dataHome from './dataHome'
import dataWhatsNew from './dataWhatsNew'
import dataDesigners from './dataDesigners'

const reducers = combineReducers({
  dataUser,
  dataHome,
  dataWhatsNew,
  dataDesigners
})

export default reducers
