const initialState = {
  data: {}
}

export default function dataUser(state = initialState, action) {
  switch (action.type) {
    case 'SET_DATA_CHAT':
      return {
        ...state,
        data: {}
      }
    case 'SET_DATA_CHAT_SUCCESS':
      return {
        ...state,
        data: action.data
      }
    default:
      return state
  }
}
