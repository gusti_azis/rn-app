const initialState = {
  data: [],
  isFetching: false,
  error: false
}

export default function dataHome(state = initialState, action) {
  switch (action.type) {
    case 'FETCHING_DATA_HOME':
      return {
        ...state,
        data: [],
        isFetching: true
      }
    case 'FETCHING_DATA_HOME_SUCCESS':
      return {
        ...state,
        data: action.data,
        isFetching: false
      }
    case 'FETCHING_DATA_HOME_FAILURE':
      return {
        ...state,
        isFetching: false,
        error: true
      }
    default:
      return state
  }
}
