import React from 'react'
import { Platform, StyleSheet, View } from 'react-native'
import { StackNavigator, TabNavigator } from 'react-navigation'
import Ionicons from 'react-native-vector-icons/Ionicons'

import HomeScreen from './screens/HomeScreen'
import WhatsNewScreen from './screens/WhatsNewScreen'
import DesignersScreen from './screens/DesignersScreen'
import ShopScreen from './screens/ShopScreen'
import AccountScreen from './screens/AccountScreen'
import SearchScreen from './screens/SearchScreen'
import CartScreen from './screens/CartScreen'
import SignInScreen from './screens/SignInScreen'
import RegisterScreen from './screens/RegisterScreen'
import ChatScreen from './screens/ChatScreen'
import ForgotPasswordScreen from './screens/ForgotPasswordScreen'
import PermissionsLocationScreen from './screens/PermissionsLocationScreen'
import PermissionsPhotosScreen from './screens/PermissionsPhotosScreen'
import PermissionsCameraScreen from './screens/PermissionsCameraScreen'

const ios = Platform.OS === 'ios'
let tabBarOptions

if (Platform.OS === 'ios') {
  tabBarOptions = {
    activeTintColor: '#212121',
    labelStyle: {
      paddingBottom: 2
    },
    style: {
      backgroundColor: '#FFFFFF'
    }
  }
}

if (Platform.OS === 'android') {
  tabBarOptions = {
    activeTintColor: '#212121',
    inactiveTintColor: '#555555',
    showIcon: true,
    upperCaseLabel: false,
    pressColor: '#212121',
    pressOpacity: 0.5,
    scrollEnabled: false,
    indicatorStyle: {
      backgroundColor: '#212121',
      position: 'absolute',
      top: 0
    },
    labelStyle: {
      fontSize: 8
    },
    style: {
      maxHeight: 56,
      backgroundColor: '#FFFFFF'
    }
  }
}

const Nav = function (title, navigation, opts) {
  const def = {
    title: title,
    headerTitle: title,
    headerBackTitle: null,
    headerRight:
      opts ? (
        <View style={styles.navCol}>
          <Ionicons onPress={() => navigation.navigate('Search')} name="ios-search-outline" size={24} style={styles.navIcon} />
          <Ionicons onPress={() => navigation.navigate('Cart')} name="ios-cart-outline" size={25} style={styles.navIcon} />
        </View>
      ) : (
        <View style={styles.navCol}>
          <Ionicons onPress={() => navigation.goBack()} name="ios-close" size={36} style={[styles.navIcon, styles.navIconClose]} />
        </View>
      ),
    headerLeft: null,
    headerStyle: styles.header,
    headerTitleStyle: styles.headerTitle,
    headerBackTitleStyle: styles.headerBackTitle,
    headerTintColor: 'black',
    gesturesEnabled: true
  }

  return Object.assign(def, opts)
}

export const AppNavigator = StackNavigator({
  Main: {
    screen: TabNavigator({
      HomeTab: {
        screen: HomeScreen,
        navigationOptions: ({ navigation }) => Nav('THE PORTER', navigation, {
          tabBarLabel: 'Home',
          tabBarIcon: ({ tintColor, focused }) => (
            <Ionicons name={focused ? 'ios-home' : 'ios-home-outline'} size={26} style={{ color: tintColor }} />
          )
        })
      },
      WhatsNewTab: {
        screen: WhatsNewScreen,
        navigationOptions: ({ navigation }) => Nav('WHAT\'S NEW', navigation, {
          tabBarLabel: 'What\'s New',
          tabBarIcon: ({ tintColor, focused }) => (
            <Ionicons name={focused ? 'ios-trending-up' : 'ios-trending-up-outline'} size={26} style={{ color: tintColor }} />
          )
        })
      },
      DesignersTab: {
        screen: DesignersScreen,
        navigationOptions: ({ navigation }) => Nav('DESIGNERS', navigation, {
          tabBarLabel: 'Designers',
          tabBarIcon: ({ tintColor, focused }) => (
            <Ionicons name={focused ? 'ios-book' : 'ios-book-outline'} size={26} style={{ color: tintColor }} />
          )
        })
      },
      ShopTab: {
        screen: ShopScreen,
        navigationOptions: ({ navigation }) => Nav('SHOP', navigation, {
          tabBarLabel: 'Shop',
          tabBarIcon: ({ tintColor, focused }) => (
            <Ionicons name={focused ? 'ios-list' : 'ios-list-outline'} size={36} style={{ color: tintColor }} />
          )
        })
      },
      AccountTab: {
        screen: AccountScreen,
        navigationOptions: ({ navigation }) => Nav('ACCOUNT', navigation, {
          tabBarLabel: 'Account',
          tabBarIcon: ({ tintColor, focused }) => (
            <Ionicons name={focused ? 'ios-person' : 'ios-person-outline'} size={30} style={{ color: tintColor }} />
          )
        })
      }
    }, {
      tabBarPosition: 'bottom',
      swipeEnabled: true,
      tabBarOptions: tabBarOptions
    })
  },
  Search: {
    screen: SearchScreen,
    navigationOptions: ({ navigation }) => Nav('SEARCH', navigation)
  },
  Cart: {
    screen: CartScreen,
    navigationOptions: ({ navigation }) => Nav('CART', navigation)
  },
  SignIn: {
    screen: SignInScreen,
    navigationOptions: ({ navigation }) => Nav('SIGN IN', navigation)
  },
  Register: {
    screen: RegisterScreen,
    navigationOptions: ({ navigation }) => Nav('REGISTER', navigation)
  },
  Chat: {
    screen: ChatScreen,
    navigationOptions: ({ navigation }) => Nav('CHAT', navigation)
  },
  ForgotPassword: {
    screen: ForgotPasswordScreen,
    navigationOptions: ({ navigation }) => Nav('FORGOT PASSWORD', navigation)
  },
  PermissionsLocation: {
    screen: PermissionsLocationScreen,
    navigationOptions: ({ navigation }) => Nav('PERMISSION LOCATION', navigation)
  },
  PermissionsPhotos: {
    screen: PermissionsPhotosScreen,
    navigationOptions: ({ navigation }) => Nav('PERMISSION PHOTOS', navigation)
  },
  PermissionsCamera: {
    screen: PermissionsCameraScreen,
    navigationOptions: ({ navigation }) => Nav('PERMISSION CAMERA', navigation)
  }
}, {
  mode: 'modal'
})

const styles = StyleSheet.create({
  navCol: {
    flex: 1,
    flexDirection: 'row'
  },
  navIcon: {
    flexDirection: 'column',
    paddingTop: ios ? 8 : 16,
    paddingRight: 12.5,
    paddingBottom: ios ? 12 : 12,
    paddingLeft: 12.5,
    justifyContent: 'center'
  },
  navIconClose: {
    paddingTop: ios ? 4 : 10
  },
  header: {
    backgroundColor: '#FFF'
  },
  headerTitle: {
    fontFamily: 'Gill Sans',
    fontWeight: '400',
    fontSize: 16,
    letterSpacing: 2
  },
  headerBackTitle: {
    color: '#212121'
  }
})

export default AppNavigator
